package com.gingermess.fastfood;

import java.util.List;

import com.gingermess.fastfood.sandwiches.SandwichImpl;
import com.gingermess.fastfood.sandwiches.SandwichService;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * todo: documentation
 * <p>
 * Created by Richard 14/10/2019
 */
public class TestSandwichService {
  
  @Test
  public void testSuccess() {
    final SandwichService sandwichService = FastFood.sandwichesPlease();
    final FastFoodSchedule s = sandwichService.from(List.of(new SandwichImpl("1"),
                                                            new SandwichImpl("2"),
                                                            new SandwichImpl("3")));
    assertThat(s.getTasks()).hasSize(6);
    assertThat(s.prettyPrint()).isEqualTo("1. 0:00 make sandwich 1\n" +
                                          "2. 1:00 serve sandwich 1\n" +
                                          "3. 1:30 make sandwich 2\n" +
                                          "4. 2:30 serve sandwich 2\n" +
                                          "5. 3:00 make sandwich 3\n" +
                                          "6. 4:00 serve sandwich 3\n" +
                                          "7. 4:30 take a well earned break!");
  }
  
  @Test
  public void testEmptyOrders() {
    final FastFoodSchedule s = FastFood.sandwichesPlease().from(List.of());
    assertThat(s.getTasks()).isEmpty();
    assertThat(s.prettyPrint()).isEqualTo("1. 0:00 slacking off, are we?");
  }
  
  @Test
  public void testAppend() {
    final FastFoodSchedule original = FastFood.sandwichesPlease().from(List.of(new SandwichImpl("1")));
    assertThat(original.getTasks()).hasSize(2);
    final FastFoodSchedule updated = FastFood.sandwichesPlease().append(new SandwichImpl("2"), original);
    assertThat(updated.getTasks()).hasSize(4);
    assertThat(updated.prettyPrint()).isEqualTo("1. 0:00 make sandwich 1\n" +
                                                "2. 1:00 serve sandwich 1\n" +
                                                "3. 1:30 make sandwich 2\n" +
                                                "4. 2:30 serve sandwich 2\n" +
                                                "5. 3:00 take a well earned break!");
  }
}
