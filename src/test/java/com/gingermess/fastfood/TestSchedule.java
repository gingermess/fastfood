package com.gingermess.fastfood;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * todo: documentation
 * <p>
 * Created by Richard 14/10/2019
 */
public class TestSchedule {

  @Test
  public void testNewSchedule() {
    final FastFoodTask first = new FastFoodTaskImpl<>(Duration.of(30, ChronoUnit.SECONDS), "apple");
    final FastFoodTask second = new FastFoodTaskImpl<>(Duration.of(3, ChronoUnit.HOURS), "banana");
    final Collection<FastFoodTask> tasks = java.util.List.of(first, second);
    final FastFoodScheduleImpl s = new FastFoodScheduleImpl(tasks);
    assertThat(s).isNotNull();
    assertThat(s.getTasks()).containsExactly(first, second);
  }
  
  @Test
  public void testPrettyPrint() {
    final FastFoodTask first = new FastFoodTaskImpl<>(Duration.of(30, ChronoUnit.SECONDS), "apple");
    final FastFoodTask second = new FastFoodTaskImpl<>(Duration.of(3, ChronoUnit.HOURS), "banana");
    final FastFoodTask third = new FastFoodTaskImpl<>(Duration.of(3, ChronoUnit.MINUTES), "cherry");
    final FastFoodScheduleImpl s = new FastFoodScheduleImpl(List.of(first, second, third));
    assertThat(s).isNotNull();
    assertThat(s.prettyPrint()).isEqualTo("1. 0:00 apple\n" +
                                          "2. 0:30 banana\n" +
                                          "3. 180:30 cherry\n" +
                                          "4. 183:30 take a well earned break!");
  }
  
  @Test
  public void testPrettyPrintNoTasks() {
    assertThat(new FastFoodScheduleImpl(List.of()).prettyPrint()).isEqualTo("1. 0:00 slacking off, are we?");
  }
}
