package com.gingermess.fastfood;

import com.gingermess.fastfood.sandwiches.BasicSandwichService;
import com.gingermess.fastfood.sandwiches.SandwichService;

/**
 * Entry point for the library. This class provides instances of fast food services.
 * <p>
 * Created by Richard 14/10/2019
 */
public class FastFood {
  
  /**
   * Get the {@link SandwichService} for creating sandwiches!
   * @return the sandwich service
   */
  public static SandwichService sandwichesPlease() {
    return new BasicSandwichService();
  }
}
