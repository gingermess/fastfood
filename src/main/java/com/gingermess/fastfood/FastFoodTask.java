package com.gingermess.fastfood;

import java.time.Duration;

/**
 * Defines a scheduled item.
 * <p>
 * Created by Richard 14/10/2019
 */
public interface FastFoodTask<T> {
  /**
   * Get the {@link Duration} that this task will take.
   * @return the duration for this task
   */
  Duration getDuration();
  
  /**
   * Get the task item itself.
   * @return the task item
   */
  T getItem();
}
