package com.gingermess.fastfood;

import java.time.Duration;

/**
 * Basic impl of {@link FastFoodTask}.
 * <p>
 * Created by Richard 14/10/2019
 */
public class FastFoodTaskImpl<T> implements FastFoodTask<T> {
  private final Duration duration;
  private final T item;
  
  public FastFoodTaskImpl(final Duration duration, final T item) {
    this.duration = duration;
    this.item = item;
  }
  
  @Override
  public Duration getDuration() {
    return duration;
  }
  
  @Override
  public T getItem() {
    return item;
  }
}
