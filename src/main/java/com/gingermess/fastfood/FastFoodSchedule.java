package com.gingermess.fastfood;

import java.util.Collection;

/**
 * Describes a sandwich creation schedule.
 * <p>
 * Created by Richard 14/10/2019
 */
public interface FastFoodSchedule {
  /**
   * Get all tasks in this schedule in order.
   * @return all tasks in this schedule
   */
  Collection<FastFoodTask> getTasks();
  
  /**
   * Print this schedule in an easy to read format to a String.
   * @return this schedule as a formatted string
   */
  String prettyPrint();
}
