package com.gingermess.fastfood;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Collections;
import java.util.Queue;

/**
 * Default implementation of {@link FastFoodSchedule}.
 * <p>
 * Created by Richard 14/10/2019
 */
public class FastFoodScheduleImpl implements FastFoodSchedule {
  private final Queue<FastFoodTask> queue;
  
  public FastFoodScheduleImpl(final Collection<FastFoodTask> tasks) {
    this.queue = new ArrayDeque<>(tasks);
  }
  
  @Override
  public Collection<FastFoodTask> getTasks() {
    return Collections.unmodifiableCollection(queue);
  }
  
  @Override
  public String prettyPrint() {
    if (queue.isEmpty()) {
      return "1. 0:00 slacking off, are we?";
    }
    final StringBuilder sb = new StringBuilder();
    Duration total = Duration.of(0, ChronoUnit.SECONDS);
    int item = 1;
    for (final FastFoodTask task : queue) {
      sb.append(item).append(". ");
      sb.append(total.toMinutes()).append(":").append(getSecondsText(total));
      sb.append(" ");
      sb.append(task.getItem());
      sb.append("\n");
      total = total.plus(task.getDuration());
      item++;
    }
    sb.append(item).append(". ").append(total.toMinutes()).append(":").append(getSecondsText(total)).append(" take a well earned break!");
    return sb.toString();
  }
  
  protected String getSecondsText(final Duration d) {
    final long seconds = d.toSeconds() - (d.toMinutes() * 60);
    return seconds == 0 ? "00" : String.valueOf(seconds);
  }
}
