package com.gingermess.fastfood.sandwiches;

import java.util.Collection;

import com.gingermess.fastfood.FastFoodSchedule;

/**
 * Provides all the sandwich scheduling you could possibly ask for*.
 * <p>
 * *in version 1.
 * <p>
 * Created by Richard 14/10/2019
 */
public interface SandwichService {
  /**
   * Append a sandwich to the end of an existing schedule, and get an update schedule.
   * @param sandwich         the sandwich to add to the schedule
   * @param existingSchedule the existing schedule
   * @return a new schedule with the new sandwich attached
   */
  FastFoodSchedule append(Sandwich sandwich, FastFoodSchedule existingSchedule);
  
  /**
   * Append a collection of sandwiches to the end of an existing schedule, and get an updated schedule.
   * @param sandwiches       the sandwiches to add
   * @param existingSchedule the existing schedule
   * @return a new schedule with the new sandwiches attached
   */
  FastFoodSchedule append(Collection<Sandwich> sandwiches, FastFoodSchedule existingSchedule);
  
  /**
   * Create a new schedule from a series of sandwiches.
   * @param sandwiches the sandwiches
   * @return a schedule
   */
  FastFoodSchedule from(Collection<Sandwich> sandwiches);
}
