package com.gingermess.fastfood.sandwiches;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import com.gingermess.fastfood.FastFoodSchedule;
import com.gingermess.fastfood.FastFoodScheduleImpl;
import com.gingermess.fastfood.FastFoodTask;
import com.gingermess.fastfood.FastFoodTaskImpl;

/**
 * Basic sandwich service that can take a list of sandwiches and produce a schedule for creating and serving them. Stateless.
 * <p>
 * Created by Richard 14/10/2019
 */
public class BasicSandwichService implements SandwichService {
  
  public BasicSandwichService() {
  }
  
  @Override
  public FastFoodSchedule append(final Collection<Sandwich> sandwiches, final FastFoodSchedule existingSchedule) {
    final Collection<FastFoodTask> newTasks = new ArrayList<>(existingSchedule.getTasks());
    sandwiches.forEach(s -> {
      newTasks.add(createMakeTask(s));
      newTasks.add(createServeTask(s));
    });
    return new FastFoodScheduleImpl(newTasks);
  }
  
  protected FastFoodTask<String> createServeTask(final Sandwich sandwich) {
    return new FastFoodTaskImpl<>(Duration.of(30, ChronoUnit.SECONDS), "serve " + sandwich);
  }
  
  protected FastFoodTask<String> createMakeTask(final Sandwich sandwich) {
    return new FastFoodTaskImpl<>(Duration.of(1, ChronoUnit.MINUTES), "make " + sandwich);
  }
  
  @Override
  public FastFoodSchedule append(final Sandwich sandwich, final FastFoodSchedule existingSchedule) {
    return append(Collections.singleton(sandwich), existingSchedule);
  }
  
  @Override
  public FastFoodSchedule from(final Collection<Sandwich> sandwiches) {
    return append(sandwiches, new FastFoodScheduleImpl(Collections.emptyList()));
  }
}
