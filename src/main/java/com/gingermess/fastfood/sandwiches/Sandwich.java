package com.gingermess.fastfood.sandwiches;

/**
 * Supposedly invented by John Montagu, 4th Earl of Sandwich, the identically named lunch food typically consists of sliced cheese, meat, or vegetables, placed
 * between two slices of bread.
 * <p>
 * Created by Richard 14/10/2019
 */
public interface Sandwich {
  String getName();
}
