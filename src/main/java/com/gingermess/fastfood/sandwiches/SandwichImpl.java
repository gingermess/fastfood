package com.gingermess.fastfood.sandwiches;

/**
 * Default implementation of {@link Sandwich}.
 * <p>
 * Created by Richard 14/10/2019
 */
public class SandwichImpl implements Sandwich {
  private final String name;
  
  public SandwichImpl(final String name) {
    this.name = name;
  }
  
  public String getName() {
    return name;
  }
  
  @Override
  public String toString() {
    return "sandwich " + getName();
  }
}
