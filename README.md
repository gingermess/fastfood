## Fast Food Library

This repository contains the source for a fast food library. 
Currently it only provides sandwich order scheduling, due to the universal appreciation for sandwiches.

### Building

Building this project requires a recent Maven and JDK 11 available on the path.

Clone this repository:

`git clone https://bitbucket.org/gingermess/fastfood.git`  

CD into the repository directory and build:

`mvn package`

In the `target/` directory will be the library JAR.

### Usage

FastFood is a library accessible via the `FastFood` factory class. To obtain an instance of the sandwich service,
use:

`SandwichService service = FastFood.sandwichesPlease();`

The service is stateless so it does not need to be retained between calls. Schedules returned 
from the service are immutable and can be created from a collection of `Sandwich` instances:

`FastFoodSchedule schedule = service.from(List.of(new SandwichImpl("BLT")));`

Each sandwich will be split into two tasks in a schedule: a make task, which takes one minute, and a 
serve task, which takes thirty seconds.   
A schedule can also have additional sandwiches appended to 
it via other methods on the service:

`FastFoodSchedule updated = service.append(new SandwichImpl("Egg"), schedule);`

Finally, a schedule can provide a formatted String that describes the orders in an easy to read manner:

`String orders = updated.prettyPrint();`

..which looks something like this:

```
1. 0:00 make sandwich BLT
2. 1:00 serve sandwich BLT
3. 1:30 make sandwich Egg
4. 2:30 serve sandwich Egg
5. 3:00 take a well earned break!
```

Enjoy your lunch!
